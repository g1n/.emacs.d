					; Package repos

(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(package-initialize)

					; THEME
(load-theme 'twilight t)

;; custom
(setq custom-file "~/.emacs.d/custom.el")
(load custom-file)

					; General configuration

;; no startup message
(setq inhibit-startup-message t)

;; no toolbar
(progn
  (if (fboundp 'tool-bar-mode) (tool-bar-mode -1))
  (menu-bar-mode -1)
  (scroll-bar-mode -1))

;; don't ask to spell out "yes"
(fset 'yes-or-no-p 'y-or-n-p)

;; show line numbers
(global-display-line-numbers-mode)
;; open links in eww
(setq browse-url-browser-function 'eww-browse-url)
;;; No cookies
(setq url-cookie-trusted-urls '()
      url-cookie-untrusted-urls '(".*"))

;; general keybindings

;;; Zooming
(global-set-key (kbd "C-=") 'text-scale-increase)
(global-set-key (kbd "C--") 'text-scale-decrease)

;;; Calendar
(global-set-key (kbd "C-x c") 'calendar)

					; org-mode
;; Enable org-mode
(require 'org)
(global-set-key (kbd "C-c l") 'org-store-link)
(global-set-key (kbd "C-c a") 'org-agenda)
(global-set-key (kbd "C-c c") 'org-capture)
(setq org-agenda-files '("~/org/gru/"
			 "~/org/roam/"
			 "~/org/roam/daily"
			 "~/org/agenda/"))
(setq org-html-validation-link nil)

;; Org Roam

;(make-directory "~/org/roam") 
(setq org-roam-directory "~/org/roam")
(setq org-roam-completion-everywhere t)
(setq org-roam-v2-ack t)
(global-set-key (kbd "C-c n l") 'org-roam-buffer-toggle)
(global-set-key (kbd "C-c n f") 'org-roam-node-find)
(global-set-key (kbd "C-c n i") 'org-roam-node-insert)
(global-set-key (kbd "C-M-i") 'completion-at-point)
;;; Org Roam dailies
(require 'org-roam-dailies)
(global-set-key (kbd "C-c n d") 'org-roam-dailies-map)
(global-set-key (kbd "C-c n d Y") 'org-roam-dailies-capture-yesterday)
(global-set-key (kbd "C-c n d T") 'org-roam-dailies-capture-tomorrow)
(org-roam-db-autosync-mode)
